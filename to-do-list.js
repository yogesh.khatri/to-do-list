// click event
var counter = 0;
document.addEventListener("click", function(e) {
        const clicked = e.target.className;
        if (clicked == "deleteBtn") {
            deleteFunction(e);
        }
        if (clicked == "checkbox") {
            if (e.target.checked == true) {
                strikeFunction(e);
            } else {
                unStrikeFunction(e);
            }
        }
    })
    // remove item from list
function deleteFunction(e) {
    let li = e.target.parentElement;
    li.parentNode.removeChild(li);
}

// strike a item
function strikeFunction(e) {
    let li = e.target.parentElement;
    var temp = li.innerText.slice(0, li.innerText.length - 1);
    const del = document.createElement('del');
    del.textContent = temp;
    li.replaceChild(del, li.childNodes[1])
}

// un-strike a item
function unStrikeFunction(e) {
    const removeStrike = e.target.parentElement;
    var temp = document.createTextNode(removeStrike.childNodes[1].innerText)
    removeStrike.replaceChild(temp, removeStrike.childNodes[1]);
}
var startPosition = 0;
var endPosition = 0;
// drag and drop function
const targetLocation = document.querySelector('#list');

targetLocation.addEventListener('dragstart', dragStart);
targetLocation.addEventListener('dragend', dragEnd);
targetLocation.addEventListener('dragover', dragOver);

function dragStart(obj) {
    console.log(obj)
}

function dragOver(e) {
    e.preventDefault();
}

function dragEnd(e) {
    console.log(e)
}
// adding element in list
const form = document.querySelector('#form');
form.addEventListener('submit', function(e) {
    // preventing default button
    e.preventDefault();
    // getting input text
    const inputText = form.querySelector('input[type="text"]').value;
    // creating checkbox
    const checkbox = document.createElement('input');
    checkbox.type = "checkbox";
    checkbox.className = "checkbox";
    // creating new list-item
    const newItem = document.createElement('li');
    newItem.className = 'list-item';
    newItem.textContent = inputText;
    newItem.setAttribute('position', counter++)
    var attribute = document.createAttribute("draggable");
    attribute.value = "true"
    newItem.setAttributeNode(attribute);
    // creating delete button
    const newBtn = document.createElement('button');
    newBtn.className = 'deleteBtn';
    newBtn.textContent = 'X';
    // append delete button to newItem
    newItem.insertBefore(checkbox, newItem.childNodes[0]);
    newItem.appendChild(newBtn);
    // append newItem at the beginning of list
    const ulList = document.querySelector('#list')
    const firstElementOfList = document.querySelector('.list-item');
    console.log(newItem)
    ulList.insertBefore(newItem, firstElementOfList);
    form.reset();
})